var CONFIG = require('../../utils/config.js')
var WxParse = require('../../utils/wxParse/wxParse.js');

var app = getApp();
Page({
  data:{
    news:{}
  },
  onLoad: function (options) {
    var that = this;
    wx.request({
      url: CONFIG.API_URL+"/content?contentId="+options.id,
      method: 'GET',
      data: {},
      header: {
        'Accept': 'application/json'
      },
      success: function(res) {
        console.log(res);

        if (res.statusCode == 200 && res.data.type == 'success') {   
          that.setData({ news: res.data.data.content}); 
          console.log(1);
          WxParse.wxParse('content', 'html', res.data.data.content.introduction, that, 25)
        } else {
          
        }
      }
    })
  },
  onReady:function(){
    // 页面渲染完成
  },
  onShow:function(){
    // 页面显示
  },
  onHide:function(){
    // 页面隐藏
  },
  onUnload:function(){
    // 页面关闭
  }
})
